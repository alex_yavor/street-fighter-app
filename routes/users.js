const express = require('express');
const router = express.Router();

const {
  getUsers,
  getUser,
  newUser,
  updateUserInfo,
  deleteUserInfo } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/', async (req, res, next) => {
  const users = await getUsers();
  console.log(users)
  if (users) {
    res.send(users);
  } else {
    res.status(400).send(`Can not find users`);
  }
});

router.get('/:id', async (req, res, next) => {
  const userId = req.params.id;
  const user = await getUser(userId);
  if (user) {
    res.send(user);
  } else {
    res.status(400).send(`Can not find user with id ${userId}`);
  }

});

router.post('/', async (req, res, next) => {
  const user = req.body;
  const answer = await newUser(user)
  res.send(answer);
});

router.put('/:id', async (req, res, next) => {
  const user = req.body;
  console.log(req.body)
  const userId = req.params.id;
  const response = await updateUserInfo(userId, user);
  res.send({ response });

});

router.delete('/:id', async (req, res, next) => {
  const userId = req.params.id;
  const response = await deleteUserInfo(userId);
  res.send(response);
});

module.exports = router;
