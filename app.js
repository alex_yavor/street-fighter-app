var express = require('express');
var cookieParser = require('cookie-parser');
const bodyPaser = require('body-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyPaser.json())
app.use(bodyPaser.urlencoded({ extended: false }))
app.use(express.static('public'));

app.use('/', indexRouter);
app.use('/user', usersRouter);

module.exports = app;
