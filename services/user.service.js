const {
  getAllUsers,
  getUserInfo,
  createUser,
  updateUser,
  deleteUser } = require("../repositories/user.repository");

const getUsers = async () => {
  try {
    const users = await getAllUsers();
    if (users) {
      return users;
    }
    else {
      return null;
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const getUser = async (userId) => {
  try {
    const user = await getUserInfo(userId);
    if (user) {
      console.log('getUser service', user)
      return user;
    }
    else {
      return null;
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const newUser = async (user) => {
  const invalidParams = InvalidUserParams(user);
  if (invalidParams) { return invalidParams }
  try {

    const response = await createUser(user);
    if (response) {

      return response;
    }
    else {
      return "Can not create user";
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const updateUserInfo = async (userId, user) => {
  const invalidParams = InvalidUserParams(user);
  if (invalidParams) { return invalidParams }
  try {
    const response = await updateUser(userId, user);

    return response;
  }
  catch (error) {
    console.log(error.message);

    return "Can not update user";
  }
}

const deleteUserInfo = async (userId) => {
  try {
    let response = await deleteUser(userId);

    return response;
  }
  catch (error) {
    console.log(error.message);

    return "Can not delete user";
  }
}

const InvalidUserParams = ({ health, attack, defense }) => {
  console.log('fff',health,defense)
  if (isNaN(health) || health < 1) {
    return "Invalid health";
  }
  if (isNaN(attack) || attack < 1) {
    return "Invalid attack";
  }
  if (isNaN(defense) || defense < 0) {
    return "Invalid defense";
  }
  return false;
}

module.exports = {
  getUsers,
  getUser,
  newUser,
  updateUserInfo,
  deleteUserInfo
};
